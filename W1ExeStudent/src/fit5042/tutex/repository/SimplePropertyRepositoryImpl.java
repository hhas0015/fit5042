/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/***
 * 
 * @author Harsha Hassan Ravindra Ravindra
 *
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository {
	
	private ArrayList<Property> agencyProperties;
	
    public SimplePropertyRepositoryImpl() {
        this.agencyProperties = new ArrayList<Property>();
    }

	@Override
	public void addProperty(Property property) throws Exception {
		
		this.agencyProperties.add(property);
		
	}

	@Override
	public Property searchPropertyById(int id) throws Exception {
		Property foundProperty = null;
		
		for(Property prop : this.agencyProperties) {
			if( id == prop.getPropertyId() ) {
				foundProperty = prop;
				break;
			}
		}
		
		return foundProperty;
		
	}

	@Override
	public List<Property> getAllProperties() throws Exception {
		return this.agencyProperties;
	}
    
}
