/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository.entities;

import java.math.BigDecimal;
import java.text.NumberFormat;

/***
 * 
 * @author Harsha Hassan Ravindra
 *
 */
//TODO Exercise 1.3 Step 1 Please refer tutorial exercise. 
public class Property {
    
	private int propertyId;
	private String propertyAddress;
	private int numberOfBedrooms;
	private int size;
	private double propertyPrice;
	
	/**
	 * @param propertyId
	 * @param propertyAddress
	 * @param numberOfBedrooms
	 * @param size
	 * @param propertyPrice
	 */
	public Property(int propertyId, String propertyAddress, int numberOfBedrooms, int size, double propertyPrice) {
		
		this.propertyId = propertyId;
		this.propertyAddress = propertyAddress;
		this.numberOfBedrooms = numberOfBedrooms;
		this.size = size;
		this.propertyPrice = propertyPrice;
	}

	/**
	 * @return the propertyId
	 */
	public int getPropertyId() {
		return propertyId;
	}

	/**
	 * @param propertyId the propertyId to set
	 */
	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}

	/**
	 * @return the propertyAddress
	 */
	public String getPropertyAddress() {
		return propertyAddress;
	}

	/**
	 * @param propertyAddress the propertyAddress to set
	 */
	public void setPropertyAddress(String propertyAddress) {
		this.propertyAddress = propertyAddress;
	}

	/**
	 * @return the numberOfBedrooms
	 */
	public int getNumberOfBedrooms() {
		return numberOfBedrooms;
	}

	/**
	 * @param numberOfBedrooms the numberOfBedrooms to set
	 */
	public void setNumberOfBedrooms(int numberOfBedrooms) {
		this.numberOfBedrooms = numberOfBedrooms;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * @return the propertyPrice
	 */
	public double getPropertyPrice() {
		return propertyPrice;
	}

	/**
	 * @param propertyPrice the propertyPrice to set
	 */
	public void setPropertyPrice(double propertyPrice) {
		this.propertyPrice = propertyPrice;
	}

	@Override
	public String toString() {
		return "Property [propertyId=" + propertyId + ", propertyAddress=" + propertyAddress + ", numberOfBedrooms="
				+ numberOfBedrooms + ", size=" + size + ", propertyPrice=" + propertyPrice + "]";
	}
	
	
	
	
	
}
