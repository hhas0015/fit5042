package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/***
 * 
 * @author Harsha Hassan Ravindra
 *
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // latest File
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
       Property property1 = new Property(1,"87 Parkes Road Melbourne VIC", 3, 540, 50000.0);
       Property property2 = new Property(2,"13 Eungella Road woodworks QueensLand", 4, 640, 70000.0);
       Property property3 = new Property(3,"40 Norton Street belrose NSW", 2, 440, 40000.0);
       Property property4 = new Property(4,"83 Ranworth Road Bateman WA", 4, 780, 90000.0);
       Property property5 = new Property(5,"98 Walter Crescent Kanahooka NSW", 2, 580, 80000.0);
       try {
		this.propertyRepository.addProperty(property1 );
		this.propertyRepository.addProperty(property2 );
		this.propertyRepository.addProperty(property3 );
		this.propertyRepository.addProperty(property4 );
		this.propertyRepository.addProperty(property5 );
	} catch (Exception e) {
		
		e.printStackTrace();
	}
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
        
    	try {
			for(int i = 0; i < this.propertyRepository.getAllProperties().size(); i++) {
				System.out.println( this.propertyRepository.getAllProperties().get(i) );
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Property ID");
        
        boolean propertyFound = false;
        
        try {
        	int searchPropertyId = input.nextInt();
        	input.nextLine();
        	
        	for(int i = 0; i < this.propertyRepository.getAllProperties().size(); i++) {
        		if( this.propertyRepository.getAllProperties().get(i).getPropertyId() == searchPropertyId ) {
        			System.out.println("Property Found");
        			System.out.println( this.propertyRepository.getAllProperties().get(i) );
        			propertyFound = true;
        			break;
        		}
				
			}
        	
        }catch(Exception e) {
        	System.out.println("invalid Property ID");
        	input.nextLine();
        }
        
        if ( !propertyFound ) {
        	System.out.println("Property Not Found");
        }
        
    }
    
    public void run() {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
