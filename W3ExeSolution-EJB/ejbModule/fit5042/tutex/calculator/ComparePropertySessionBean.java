package fit5042.tutex.calculator;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty {
	
	private Set<Property> propertySet;
	
	
	public ComparePropertySessionBean() {
		propertySet = new HashSet<>();
	}
	
	
	@Override
	public void addProperty(Property property) {
		propertySet.add(property);

	}

	@Override
	public void removeProperty(Property property) {
		 
		for(Property prop : propertySet) {
			
			if( prop.getPropertyId() ==  property.getPropertyId()) {
				propertySet.remove(prop);
				break;
			}
			
		}

	}

	@Override
	public int getBestPerRoom() {
		
		double bestRoomPrice = 100000000.0;
		int bestRoomId = 0;
		for(Property prop : propertySet) {
			int numberOfRooms = prop.getNumberOfBedrooms();
			double price = prop.getPrice();
			double pricePerRoom = price / numberOfRooms;
			
			if( pricePerRoom < bestRoomPrice ) {
				bestRoomId = prop.getPropertyId();
			}
			
		}
		
		return bestRoomId;
		
	}

}
