package fit5042.tutex.calculator;

import fit5042.tutex.repository.entities.Property;

public interface CompareProperty {
	
	void addProperty(Property property);
	
	void removeProperty(Property property);
	
	int getBestPerRoom();

}
